const numbers = [1, 2, 3, 4, 5, 5];
const testData = [
    'Index: 0, Number: 1',
    'Index: 1, Number: 2',
    'Index: 2, Number: 3',
    'Index: 3, Number: 4',
    'Index: 4, Number: 5',
    'Index: 5, Number: 5'
]
const getFun = require('../root/each')
const forAll = getFun.each
let outputData = []

function alert(item, index) {
    outputData.push(`Index: ${index}, Number: ${item}`);
}

forAll(numbers, alert);
if(JSON.stringify(outputData) === JSON.stringify(testData)) {
    console.log('Test Passed!')
} else {
    console.log('Test Failed')
}