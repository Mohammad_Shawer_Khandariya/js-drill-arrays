const numbers = [1, 2, 3, 4, 5, 5];
const getFun = require('../root/find')
const findAll = getFun.find

function mod2(item) {
    return(item % 2 === 0);
}

const testData = numbers.find(mod2);
const outputData = findAll(numbers, mod2);

if(testData === outputData) {
    console.log('Test Passed!')
} else {
    console.log('Test Failed!')
}