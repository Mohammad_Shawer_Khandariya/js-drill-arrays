const numbers = [1, 2, 3, 4, 5, 5];
const getFun = require('../root/filter')
const filterAll = getFun.filter

function mod2(item) {
    return(item % 2 === 0);
}

const testData = numbers.filter(mod2);
const outputData = filterAll(numbers, mod2);

if(JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!')
} else {
    console.log('Test Failed!')
}