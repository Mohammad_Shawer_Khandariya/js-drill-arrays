const numbers = [1, 2, 3, 4, 5, 5];
const getFun = require('../root/map')
const mapAll = getFun.map

function square(item) {
    return(item * item);
}

const testData = numbers.map(square)
const outputData = mapAll(numbers, square);

if(JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!')
} else {
    console.log('Test Failed!')
}