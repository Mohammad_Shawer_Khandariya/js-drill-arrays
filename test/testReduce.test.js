const numbers = [1, 2, 3, 4, 5, 5];
const getFun = require('../root/reduce')
const reduceAll = getFun.reduce

function sum(item, initialValue) {
    return(item + initialValue);
}

const testData = numbers.reduce(sum, 5)

const outputData = reduceAll(numbers, sum, 5);

if(outputData === testData) {
    console.log('Test Passed!')
} else {
    console.log('Test Failed!')
}