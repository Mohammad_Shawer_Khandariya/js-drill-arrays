const nestedArray = [1, [2], [[3]], [[[4]]]];
const getFun = require('../root/flatten')
const flattenAll = getFun.flatten

const testData = nestedArray.flat(3);
const outputData = flattenAll(nestedArray);

if(JSON.stringify(testData) === JSON.stringify(outputData)) {
    console.log('Test Passed!')
} else {
    console.log('Test Failed!')
}