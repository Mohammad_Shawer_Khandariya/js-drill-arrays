function each(elements, cb) {
    if(typeof (elements) !== 'object') {
        return('Invalid Input');
    }

    let result = []
    for (let index = 0; index < elements.length; index++) {
        cb(elements[index], index);
    }
}

module.exports = {
    each
}