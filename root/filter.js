function filter(elements, cb) {
    if(typeof (elements) !== 'object') {
        return('Invalid Input');
    }

    let result = []
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index])) {
            result.push(elements[index]);
        }
    }
    return(result);
}

module.exports = {
    filter
}