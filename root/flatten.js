function flatten(arr, depth) {
    console.log('I am in.')
    let result = [];
    arr.forEach(item => {
        if(Array.isArray(item)){
            const res = flatten(item, (depth - 1));
            res.forEach(item => result.push(item));
        } else {
            result.push(item);
        }
    });
    return(result);
}

module.exports = {
    flatten
}