function map(elements, cb) {
    if(typeof (elements) !== 'object') {
        return('Invalid Input');
    }

    let mapArray = [];
    for (let index = 0; index < elements.length; index++) {
        mapArray.push(cb(elements[index], index));
    }
    return(mapArray);
}

module.exports = {
    map
}