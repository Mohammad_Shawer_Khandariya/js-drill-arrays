function reduce(elements, cb, startingValue) {
    if(typeof (elements) !== 'object') {
        return('Invalid Input');
    }

    let index = 0;
    if (typeof (startingValue) === 'undefined') {
        index = 1;
        startingValue = elements[0]
    }

    for (index; index < elements.length; index++) {
        startingValue = cb(elements[index], startingValue);
    }
    return(startingValue);
}

module.exports = {
    reduce
}